<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(array('prefix'=>'admin','middleware' => ['auth']), function ()
{
    Route::get('/', 'CategoryController@index');
    Route::get('/product/{id}/deactivate','ProductController@deactivate')->name('product.deactivate');
    Route::get('/product/{id}/activate','ProductController@activate')->name('product.activate');
    Route::resource('category','CategoryController');
    Route::resource('product','ProductController');

});

