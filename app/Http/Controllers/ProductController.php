<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Product;
use App\Category;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $products= DB::table('products')->join('categories', 'products.category_id', '=', 'categories.id')
            ->select('products.*','categories.category_name')
           ->get();

//        $enterprises= serialize($enterprisesObject);
        return view('product.product',compact('products'))
            ->with('i', (request()->input('page', 1) - 1) * 5);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        return view('product.add_product',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'product_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'name' => 'required|string|unique:Products',
            'original_price' => 'required',
            'category_id'   =>  'required',
        ]);
        
        $imageName;
        if ($request->hasFile('product_image')) {
            $image = $request->file('product_image');
            $imageName = $image->getClientOriginalName();
            $destinationPath = public_path('/images/products');
            $image->move($destinationPath, $imageName);
    
        }
        
        product::create([
            'name'=>$request->input('name'),
            'image_name' =>$imageName,
            'original_price' =>$request->input('original_price'),
            'discount_price' => $request->input('discount_price'),
            'description'=>$request->input('description'),
            'category_id'=> $request->input('category_id'),
            'isAvailable'=> true,
        ]);
        
        return redirect()->route('product.index')
            ->with('success', 'Product created successfully');

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        // $product = Product::find($id);
//        return "hello";

        // return view('enterprise.reserve-msg', compact('enterprise', $enterprise))->with('status', 'Reserve Enterprise');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        // request()->validate([
        //     //            'EnterpriseName' => 'required|string|unique:EnterpriseAccount',
        //                 'EnterpriseContactnumber' => 'required',
        //                 'EnterpriseEmail' => 'required|email',
        //                 'EnterpriseAddress' => 'required',
        //             ]);
            
        //             enterpriseaccount::find($id)->update($request->all());
            
        //             return redirect()->route('enterprise.index')
        //                 ->with('success', 'Article updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function deactivate($id)
    {
        Product::find($id)->update(['isAvailable'=> false]);
            
        return redirect()->route('product.index')
        ->with('success', 'Product '.$id .' Deactivated successfully');
       
    }

    public function activate($id)
    {
        Product::find($id)->update(['isAvailable'=> true]);
            
        return redirect()->route('product.index')
        ->with('success', 'Product '.$id .' Activated successfully');
       
    }

    public function getProducts()
    {
        // $userId=User::where('EnterpriseId',$id)->first()->id;

        $products = Product::where('isAvailable',true)
                    ->orderBy('created_at', 'desc')
                    ->get();

        // >orderBy('name', 'desc')
        //        ->take(10)
        //        ->get();

        return response()->json($products, 200);
       
    }

    // private function imageUpload($file)
    // {
    //     if ($request->hasFile('input_img')) {
    //         if($request->file('input_img')->isValid()) {
    //             try {
    //                 $file = $request->file('input_img');
    //                 $name = rand(11111, 99999) . '.' . $file->getClientOriginalExtension();
        
    //                 # save to DB
    //                 $tickes = Users::create(['imagePath' => 'storage/'.$name]);
        
    //                 $request->file('input_img')->move("storage", $name);

    //             } catch (Illuminate\Filesystem\FileNotFoundException $e) {
        
    //             }
    //         }
    //     }
    // }

    // public function fileUpload(Request $request) {

    //     $this->validate($request, [
    //         'product_image' => 'required|image|mimes:jpeg,png,jpg|max:2048',
    //     ]);
    
    //     if ($request->hasFile('product_image')) {
    //         $image = $request->file('input_img');
    //         $name = $image->getClientOriginalName().'.'.$image->getClientOriginalExtension();
    //         $destinationPath = public_path('/images');
    //         $image->move($destinationPath, $name);
    //         $this->save();
    
    //         return back()->with('success','Image Upload successfully');
    //     }
    // }
}
