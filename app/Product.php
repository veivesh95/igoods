<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    // public $table = 'products';

    protected $fillable = [
        'name', 'category_id', 'description', 'original_price','discount_price','image_name','isAvailable',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category','id');
    }
}