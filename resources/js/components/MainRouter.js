import React, { Component } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';

import Welcome from './Welcome';

class MainRouter extends Component {
    render() {
        return (
            <main>
                <Switch>
                    <Route exact path='/' component={Welcome} />
                </Switch>
            </main>
        );
    }
}

export default withRouter(MainRouter);
