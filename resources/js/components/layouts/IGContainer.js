import React, { Fragment } from 'react';
import { Container } from 'reactstrap';

const IGContainer = props => {
    return (
        <Fragment>
            <Container className='ig-container'>{props.children}</Container>
        </Fragment>
    );
};

export default IGContainer;
