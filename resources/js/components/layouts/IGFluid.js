import React, { Fragment } from 'react';
import { Container } from 'reactstrap';

const IGFluid = props => {
    return (
        <Fragment>
            <Container fluid className='my-fluid-container container-fluid'>
                {props.children}
            </Container>
        </Fragment>
    );
};

export default IGFluid;
