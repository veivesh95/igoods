import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, NavbarToggler, Collapse, Nav, NavItem } from 'reactstrap';

export default class Header extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        };

        // this.toggle = this.toggle.bind(this);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    render() {
        return (
            <Navbar
                className='navbar-laravel'
                // color='light'
                light
                expand='md'
            >
                <Link className='navbar-brand' to='/'>
                    <img
                        className='logo'
                        src={require('../../logo/logo.png')}
                    />
                </Link>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className='ml-auto' navbar>
                        <NavItem hidden>
                            <Link className='nav-link' to='/'>
                                Indian Goods
                            </Link>
                        </NavItem>
                        <NavItem hidden>
                            <Link className='nav-link' to='/'>
                                Search
                            </Link>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
        );
    }
}
