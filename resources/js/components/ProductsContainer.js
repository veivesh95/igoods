import React, { Component } from "react";
import Product from "./Product";

class ProductsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = { products: this.props.products };
    }

    render() {
        const { products } = this.state;
        return (
            <div className="products-container">
                {products.map((product, i) => {
                    return (
                        <Product
                            key={i}
                            {...product}
                            onClick={this.props.onClick}
                        />
                    );
                })}
            </div>
        );
    }
}

export default ProductsContainer;
