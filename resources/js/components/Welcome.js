import React, { Component } from "react";
import axios from 'axios';
import IGFluid from "./layouts/IGFluid";
import { Row } from "reactstrap";
import IGFluidJumbo from "./elements/IGFluidJumbo";
import IGJumbo from "./elements/IGJumbo";
import IGContainer from "./layouts/IGContainer";
import Product from "./Product";
import ProductsContainer from "./ProductsContainer";
import React_Carousel from "./elements/React_Carousel";

// const products = [
//     {
//         name: "Chocos 30g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "30.00",
//         discount_price: "10.00",
//         image_name: "1.jpg"
//     },
//     {
//         name: "Chocos 500g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "500.00",
//         image_name: "3.jpg"
//     },
//     {
//         name: "Chocos 30g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "30.00",
//         discount_price: "10.00",
//         image_name: "4.jpg"
//     },
//     {
//         name: "Chocos 500g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "500.00",
//         image_name: "9.jpg"
//     },
//     {
//         name: "Chocos 30g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "30.00",
//         discount_price: "10.00",
//         image_name: "8.jpg"
//     },
//     {
//         name: "Chocos 500g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "500.00",
//         image_name: "7.png"
//     },
//     {
//         name: "Chocos 30g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "30.00",
//         discount_price: "10.00",
//         image_name: "10.jpg"
//     },
//     {
//         name: "Chocos 500g Awesome crunch",
//         description: "Quick brown fox jumped over the fence",
//         original_price: "500.00",
//         image_name: "12.jpg"
//     }
// ];

class Welcome extends Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            isLoading: true
        };

        this.onClick = this.onClick.bind(this);
    }

    componentDidMount(){
       
    axios.get('/api/ig-products')
      .then(res => {
        const products = res.data;
        this.setState({ 
            products:products,
            isLoading: false
         });
      })
      .catch(error => console.log(error))
        
    }

    onClick() {
        console.log(new Date());
    }

    render() {

        console.log(this.state.products);
        const isLoading = this.state.isLoading;

        return (
            <div>
            <div className="slider"> 
            <React_Carousel />
            </div>
            <React.Fragment>
                {!isLoading ? (

                <IGContainer>
                    <ProductsContainer
                        products={this.state.products}
                        onClick={this.onClick}
                    />
                </IGContainer>)
                :''
            }
            </React.Fragment>
            </div>
        );
    }
}

export default Welcome;
