import React from "react";
import { Card, CardBody, CardText, CardTitle, CardImg } from "reactstrap";

const ProductElement = props => {
    console.log(props);

    let element = null;

    let imageName = props.image_name;
    let image = require(`../../../public/images/products/${props.image_name}`);

    element = (
        <Card className="product" onClick={props.onClick}>
            <CardImg
                className="product-image"
                top
                // width="100%"
                src={require(`../../../public/images/products/${
                    props.image_name
                }`)}
                alt="Card image cap"
            />
            <CardBody>
                <CardTitle className="product-title">{props.name}</CardTitle>
                <CardText className="product-description">{props.description}</CardText>
                {props.original_price && props.discount_price ? (
                    <CardText className="product-price">
                        <small className="text-muted">
                            LKR {props.discount_price}
                        </small>
                        <span> </span>
                        <small
                            className="text-muted"
                            style={{ textDecoration: "line-through" }}
                        >
                            {props.original_price}
                        </small>
                    </CardText>
                ) : props.original_price ? (
                    <CardText className="product-price">
                        <small className="text-muted">
                            LKR {props.original_price}
                        </small>
                    </CardText>
                ) : (
                    <CardText className="product-price">
                        <small className="text-muted">
                            Price is not available at the moment
                        </small>
                    </CardText>
                )}
            </CardBody>
        </Card>
    );

    return <React.Fragment>{element}</React.Fragment>;
};

export default ProductElement;
