import styled from 'styled-components';
import { Button } from 'reactstrap';
import React from 'react';

const IGButton = props => {
    let element = null;

    switch (props.buttontype) {
        case 'primary':
            element = <Button color='primary'>{props.text}</Button>;
            break;

        default:
            element = <Button>{props.text}</Button>;
            break;
    }

    return <div>{element}</div>;
};

export default IGButton;
