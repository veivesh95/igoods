@extends('layouts.admin_layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Category
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol>
</section>

<!-- Main content -->
<section style="background-color:white" class="content container-fluid">


        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ $message }}</p>
            </div>
    @endif



                    <div class="col-sm-2" style="margin-top: 4%;margin-bottom: 2%">
            <button id="createEnterprise" type="button"  class="btn btn-block btn-success " data-toggle="modal" data-target=".bs-modal-ce">
                <i class="glyphicon glyphicon-plus"></i>
                <b>&nbsp;&nbsp;CREATE</b>
            </button>

        </div>


        
        <table id="SMS" class="table table-bordered table-hover dataTable" role="grid">
            <thead>
            <tr role="row">
                <th>Number</th>
                <th >Category Id</th>
                <th>Category Name</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($categories as $category)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $category->id}}</td>
                <td>{{ $category->category_name}}</td>
                <td>
                    <a href="{{ route('category.edit',$category->id) }}" class="btn btn-sm btn-primary"  title="Edit" ><i class="glyphicon glyphicon-pencil"></i> Edit  </a>
                    <a href="{{ route('category.destroy',$category->id) }}" class="btn btn-sm btn-danger"  title="Reserve" ><i class="glyphicon glyphicoen-pencil"></i> Delete  </a>

                </td>
            </tr>
            @endforeach

            </tbody>
        </table>


    {!! $categories->links() !!}

</section>


@include('category.add_category')
@endsection

@if ($errors->any())

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

        $(function($) {

            $('#createEnterprise\n').click();
        })

    </script>

@endif

