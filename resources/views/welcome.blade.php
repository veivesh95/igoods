<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Indian Goods</title>

        <!-- CSRF -->
        <meta name="csrf-token" content="{{csrf_token()}}">

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet" type="text/css">

        <!-- Style -->
        <link rel="stylesheet" href="{{ asset('css/app.css' )}}" type="text/css">

    </head>
    <body>
      <div id="root"></div>

      <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
