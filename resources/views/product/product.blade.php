@extends('layouts.admin_layout')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        View Product
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
    </ol>
</section>

<!-- Main content -->
<section style="background-color:white" class="content container-fluid">


        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ $message }}</p>
            </div>
    @endif



                    <div class="col-sm-2" style="margin-top: 4%;margin-bottom: 2%">

                    </div>


        
        <table id="SMS" class="table table-bordered table-hover dataTable" role="grid">
            <thead>
            <tr role="row">
                <th>Number</th>
                <th >Product Name</th>
                <th>Category</th>
                <th>Status</th>
                <th >Description</th>
                <th >Original Price</th>
                <th >Discount Price</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($products as $product)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $product->name}}</td>
                <td>{{ $product->category_name}}</td>

                @if ($product -> isAvailable)
                    <td><span class="label label-primary">ACTIVE</span></td>
                @else
                <td><span class="label label-warning">INACTIVE</span></td>
                @endif

                <td>{{ $product->description}}</td>
                <td>{{ $product->original_price}}</td>
                <td>{{ $product->discount_price}}</td>
                <td>
                    <a href="{{ route('product.edit',$product->id) }}" class="btn btn-sm btn-primary"  title="Edit" ><i class="glyphicon glyphicon-pencil"></i> Edit  </a>

                @if ($product -> isAvailable)
                
                    <a href="{{ route('product.deactivate',$product->id) }}" class="btn btn-sm btn-success"  title="Reserve" ><i class="glyphicon glyphicoen-pencil"></i> Disable  </a>
                
                @else
                
                    <a href="{{ route('product.activate',$product->id) }}" class="btn btn-sm btn-success"  title="Reserve" ><i class="glyphicon glyphicoen-pencil"></i> Activate  </a>
                
                @endif
    
                </td>
            </tr>
            @endforeach

            </tbody>
        </table>



</section>


@endsection

@if ($errors->any())

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

        $(function($) {

            $('#createEnterprise\n').click();
        })

    </script>

@endif

