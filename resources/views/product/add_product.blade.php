@extends('layouts.admin_layout')
@section('content')

    <section class="content-header">
        <h1>
            Add Product
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section style="background-color:white" class="content container-fluid">

<div class="row">
            
<form class="form-horizontal" method="POST" enctype="multipart/form-data" action="{{ route('product.store')}}">
                {{ csrf_field() }}
                <div class="modal-body form">

                    <div class="form-body">

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Product Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="category_name" class="col-md-4 control-label">Original Price</label>

                            <div class="col-md-6">
                                <select id="category_id" name="category_id" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="dropdown">
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                    @endforeach
                                    </select>
                            </div>

                        </div>

                        <div class="form-group{{ $errors->has('original_price') ? ' has-error' : '' }}">
                            <label for="original_price" class="col-md-4 control-label">Original Price</label>

                            <div class="col-md-6">
                                <input id="original_price" type="number" class="form-control" name="original_price" value="{{ old('original_price') }}" required autofocus>

                                @if ($errors->has('original_price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('original_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('discount_price') ? ' has-error' : '' }}">
                            <label for="discount_price" class="col-md-4 control-label">Discount Price</label>

                            <div class="col-md-6">
                                <input id="discount_price" type="number" class="form-control" name="discount_price" value="{{ old('discount_price') }}" autofocus>

                                @if ($errors->has('discount_price'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('discount_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Description</label>

                            <div class="col-md-6">
                                <textarea id="description" type="text" class="form-control" name="description" value="{{ old('description') }}" autofocus></textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('product_image') ? ' has-error' : '' }}">
                            <label for="product_image" class="col-md-4 control-label">Image</label>

                            <div class="col-md-6">
                                <input type="file" id="product_image" name="product_image" class="form-control" value="{{ old('product_image') }}" autofocus>

                                @if ($errors->has('product_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('product_image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                    </div>

                </div>
                <div class="modal-footer">


                    <button type="submit"  class="btn btn-primary">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                </div>
            </form>

</div>
    </section>
@endsection