<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('name',100)->unique();
            $table->text('description')->nullable();	
            $table->float('original_price')->nullable();
            $table->float('discount_price')->nullable();
            $table->string('image_name',100)->unique()->nullable();
            $table->boolean('isAvailable')->default(1);
            $table->timestamps();
        });

        Schema::table('products', function($table)
        {
            $table->foreign('category_id')-> references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
